package repositoriesImp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import repositories.IRepositoryCatalog;
import unitOfWork.IUnitOfWork;
import unitOfWork.UnitOfWork;

public class RepositoryCatalogProvider {


	private static String url="jdbc:hsqldb:hsql://localhost/testdb";
	
	public static IRepositoryCatalog catalog()
	{

		try {
			Connection connection = DriverManager.getConnection(url);
			IUnitOfWork uow = new UnitOfWork(connection);
			IRepositoryCatalog catalog = new RepositoryCatalog(connection, uow);
		
			return catalog;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}