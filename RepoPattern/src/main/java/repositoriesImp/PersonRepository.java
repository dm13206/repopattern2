package repositoriesImp;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import entities.Entity;
import entities.Person;
import repositories.IEntityBuilder;
import unitOfWork.IUnitOfWork;
import unitOfWork.IUnitOfWorkRepository;

public class PersonRepository 
	extends Repository<Person>{

	protected PersonRepository(Connection connection,
			IEntityBuilder<Person> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
	}
	
	SimpleDateFormat format= new SimpleDateFormat("dd-MM-yyyy");

	@Override
	protected void setUpUpdateQuery(Person entity) throws SQLException {
		update.setString(1, entity.getFirstName());
		update.setString(2, entity.getSurname());
		update.setString(3, entity.getPesel());
		update.setString(4, entity.getEmail());
		update.setString(5, entity.getNip());
		update.setDate(6, (Date) entity.getDateOfBirth());
		update.setInt(7, entity.getId());
	}

	@Override
	protected void setUpInsertQuery(Person entity) throws SQLException {
		insert.setString(1, entity.getFirstName());
		insert.setString(2, entity.getSurname());
		insert.setString(3, entity.getPesel());
		insert.setString(4, entity.getEmail());
		insert.setString(5, entity.getNip());
		insert.setString(6, format.format(entity.getDateOfBirth()));
	}

	@Override
	protected String getTableName() {
		return "person";
	}

	@Override
	protected String getUpdateQuery() {
		return "update person set (name,surname,pesel,email,nip,dob)=(?,?,?,?,?,?)"
				+ "where id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "insert into person(name,surname,pesel,email,nip,dob)=(?,?,?,?,?,?)";
	}

	public void commit() {
		// TODO Auto-generated method stub
		
	}

	public void rollback() {
		// TODO Auto-generated method stub
		
	}

	public void markAsNew(Entity entity, IUnitOfWorkRepository repository) {
		// TODO Auto-generated method stub
		
	}

	public void markAsDirty(Entity entity, IUnitOfWorkRepository repository) {
		// TODO Auto-generated method stub
		
	}

	public void markAsDeleted(Entity entity, IUnitOfWorkRepository repository) {
		// TODO Auto-generated method stub
		
	}
	
}