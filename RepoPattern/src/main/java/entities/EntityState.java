package entities;

public enum EntityState {
	New, Modified, Unchanged, Deleted
}